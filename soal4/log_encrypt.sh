#!/bin/bash

input="/var/log/syslog"
path=`readlink -f "${BASH_SOURCE:-$0}"`
DIR_PATH=`dirname $path`

# Crontab
crontab -l > run_cron
echo "0 */2 * * * sudo $DIR_PATH/log_encrypt.sh" >> run_cron
crontab run_cron

output="$DIR_PATH/$(date +"%H:%M_%d:%m:%Y").txt"
touch "$output"

key=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
shift=$(date +%H)%26

tr "${key:0:26}${key:52:26}" "${key:${shift}:26}${key:(52+${shift}):26}" < "$input" > "$output"
