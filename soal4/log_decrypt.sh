path=`readlink -f "${BASH_SOURCE:-$0}"`
DIR_PATH=`dirname $path`
filename=$(ls -1t *.txt | head -1)

input="$DIR_PATH/$filename"
output="$DIR_PATH/$(basename "${filename%.*}")_decrypted.txt"

mkdir -p "decrypted"
touch "$output"

shift=$(expr $(echo "$filename" | cut -c1-2))

key=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ

tr "${key:${shift}:26}${key:(${shift}+52):26}" "${key:0:26}${key:52:26}" < "$input" > "$output"

