#!/bin/bash

cd $PWD
crontab -l > run_cron
echo "0 */10 * * * cd $PWD && ./download.sh" >> run_cron
echo "0 0 * * * cd $PWD && ./zipp.sh" >> run_cron
crontab run_cron

echo '#!/bin/bash
cd $PWD
# membuat folder awal dengan nama "kumpulan_$i"
i=1
while [ -d "kumpulan_$i" ];
do
    (( i++ ))
done
folder_name="kumpulan_$i"
mkdir "$folder_name"

# set jumlah download sesuai dengan jam
hour=$(date +%H)
if [ $hour -eq 0 ]
then
    size=1
else
    size=$hour
fi

# loop untuk melakukan download
for (( i=1; i<=$size; i++ ))
do
    # set nama file
    file_name="perjalanan_$i.jpg"    
    # download gambar dari interne
    wget -O "$folder_name/$file_name" https://source.unsplash.com/random/800x600/?indonesia
done' > download.sh

chmod +x download.sh

echo '#!/bin/bash
cd $PWD
# membuat zip file setiap 1 hari
folder_num=$(ls -d kumpulan_* | wc -l)
zip_folder=$(ls -d devil_* | wc -l)
zip_name="devil_$(($zip_folder+1)).zip"
for ((i=1; i<=$folder_num; i++))
do
    zip -r $zip_name kumpulan_$i
    rm -r kumpulan_$i
done' > zipp.sh

chmod +x zipp.sh
