# Praktikum Sistem Operasi (Modul 1)

Authored by :
- Thariq Agfi Hermawan (5025211215)
- Darren Prasetya (5025211162)
- Dewangga Dika (5025211109)



## List of Contents

 - [Soal 1](#soal-1)
   - [Bagian A](#bagian-a)
   - [Bagian B](#bagian-b)
   - [Bagian C](#bagian-c)
   - [Bagian D](#bagian-d)
 - [Soal 2](#soal-2)
   - [kobeni_liburan.sh](#kobeni_liburan.sh)
 - [Soal 3](#soal-3)
   - [louis.sh](#louis.sh)
   - [retep.sh](#retep.sh)
 - [Soal 4](#soal-4)


### Soal 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi :

#### Bagian A
---

Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu,
Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas
dengan ranking tertinggi di Jepang.
```bash
awk -F, '$4=="Japan" {print $2}' 2023\ QS\ World\ University\ Rankings.csv | head -5 | sort
```

**Penjelasan**

1. `awk -F,`    : Menentukan operator pemisah antar kolom atau delimiter menggunakan `,`
2. `$4=="Japan"`: Melakukan filter pada kolom ke-4 yang bernilai `Japan`
3. `{print $2}` : Mencetak output kolom ke-2 (Nama universitas) setelah filter
4. `2023\ QS\ World\ University\ Rankings.csv` : Path file yang yang akan dilakukan filter oleh step 1,2, dan 3
5. `head -5`    : Memilih 5 data teratas yang telah di filter
6. `sort`       : Melakukan sorting secara ascending dari hasil filter

**Output**
```bash
The University of Tokyo
Kyoto University
Tokyo Institute of Technology (Tokyo Tech)
Osaka University
Tohoku University

```

#### Bagian B
---

Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.

```bash
awk -F, '$4=="Japan" {print $9}' 2023\ QS\ World\ University\ Rankings.csv | head -5 | sort | head -1
```

**Penjelasan**

1. `awk -F,`    : Menentukan operator pemisah antar kolom atau delimiter menggunakan `,`
2. `$4=="Japan"`: Melakukan filter pada kolom ke-4 yang bernilai `Japan`
3. `{print $9}` : Mencetak output kolom ke-9 `fsr score` setelah filter
4. `2023\ QS\ World\ University\ Rankings.csv` : Path file yang yang akan dilakukan filter oleh step 1,2, dan 3
5. `head -5`    : Memilih 5 data teratas yang telah di filter
6. `sort`       : Melakukan sorting secara ascending dari hasil filter
7. `head -1`    : Mengambil data teratas/terendah dari hasil filter dan sort

**Output**
```bash
67.4
```

#### Bagian C
---

Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

```bash
awk -F, '$4=="Japan" {print $2}' 2023\ QS\ World\ University\ Rankings.csv | sort -n | head -10
```

**Penjelasan**

1. `awk -F,`    : Menentukan operator pemisah antar kolom atau delimiter menggunakan `,`
2. `$4=="Japan"`: Melakukan filter pada kolom ke-4 yang bernilai `Japan`
3. `{print $2}` : Mencetak output kolom ke-2 (Nama universitas) setelah filter
4. `2023\ QS\ World\ University\ Rankings.csv` : Path file yang yang akan dilakukan filter oleh step 1,2, dan 3
5. `head -5`    : Memilih 5 data teratas yang telah di filter
6. `sort -n`       : Melakukan sorting secara ascending dari hasil filter dan diurutkan secara numerik (karena dalam kolom itu merupakan tipe data string)
7. `head -10`    : Mengambil 10 data teratas dari hasil filter dan sort

**Output**
```bash
33 The University of Tokyo
90 Keio University
127 Waseda University
169 Hitotsubashi University
188 Tokyo University of Science
201 Kyoto University
367 Nagoya University
377 Tokyo Institute of Technology (Tokyo Tech)
379 International Christian University
543 Kyushu University
```

#### Bagian D
---

Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

```bash
grep -i keren  2023\ QS\ World\ University\ Rankings.csv | cut -d',' -f2
```

**Penjelasan**

1. `grep -i keren` : Mencari kata tertentu `(keren)` dan `-i` untuk *case-insensitive*
2. `2023\ QS\ World\ University\ Rankings.csv` : Path file yang yang akan dilakukan filter oleh step 1
3. `cut -d','` : memisahkan data dan `-d','` koma sebagai tanda pemisah antar field
4. `-f2` : data yang diambil adalah field ke-2 setiap baris

**Output**
```bash
Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren)
```

## Soal 2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
    - File yang didownload memilki format nama `perjalanan_NOMOR.FILE` Untuk NOMOR.FILE, adalah urutan file yang download (`perjalanan_1`, `perjalanan_2`, dst)
    - File batch yang didownload akan dimasukkan ke dalam folder dengan format nama `kumpulan_NOMOR.FOLDER` dengan NOMOR.FOLDER adalah urutan folder saat dibuat (`kumpulan_1`, `kumpulan_2`, dst)

- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip `devil_NOMOR ZIP` dengan NOMOR.ZIP adalah urutan folder saat dibuat (`devil_1`, `devil_2`, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

### kobeni_liburan.sh
```bash
#!/bin/bash

cd $PWD
crontab -l > run_cron
echo "0 */10 * * * cd $PWD && ./download.sh" >> run_cron
echo "0 0 * * * cd $PWD && ./zipp.sh" >> run_cron
crontab run_cron

echo '#!/bin/bash
cd $PWD
# membuat folder awal dengan nama "kumpulan_$i"
i=1
while [ -d "kumpulan_$i" ];
do
    (( i++ ))
done
folder_name="kumpulan_$i"
mkdir "$folder_name"

# set jumlah download sesuai dengan jam
hour=$(date +%H)
if [ $hour -eq 0 ]
then
    size=1
else
    size=$hour
fi

# loop untuk melakukan download
for (( i=1; i<=$size; i++ ))
do
    # set nama file
    file_name="perjalanan_$i.jpg"    
    # download gambar dari interne
    wget -O "$folder_name/$file_name" https://source.unsplash.com/random/800x600/?indonesia
done' > download.sh

chmod +x download.sh

echo '#!/bin/bash
cd $PWD
# membuat zip file setiap 1 hari
folder_num=$(ls -d kumpulan_* | wc -l)
zip_folder=$(ls -d devil_* | wc -l)
zip_name="devil_$(($zip_folder+1)).zip"
for ((i=1; i<=$folder_num; i++))
do
    zip -r $zip_name kumpulan_$i
    rm -r kumpulan_$i
done' > zipp.sh

chmod +x zipp.sh
```
**Penjelasan Keseluruhan**
1. Script pertama akan mengambil direktori saat ini sebagai working directory dengan menggunakan `cd $PWD`
2. Kemudian akan membuat file `run_cron` lalu menambahkan 2 baris untuk menjalankan file `download.sh` setiap 10 jam dan `zipp.sh` setiap 24 jam atau 1 hari
3. Selanjutnya script akan membuat file baru `download.sh` kemudian memberikan izin menggunakan `chmod +x download.sh`
4. Selanjutnya script akan membuat file baru `zipp.sh` kemudian memberikan izin menggunakan `chmod +x zipp.sh`

**Penjelasan download.sh**
```bash
cd $PWD
```
Mengambil direktori saat ini sebagai working directory
```bash
i=1
while [ -d "kumpulan_$i" ];
do
    (( i++ ))
done
folder_name="kumpulan_$i"
mkdir "$folder
```
Baris ini membuat folder baru dengan nama `kumpulan_x`, dimana x adalah angka yang bertambah satu setiap kali folder dengan nama yang sama ditemukan. Jadi, script akan mencari folder dengan nama `kumpulan_1`, kemudian `kumpulan_2`, dan seterusnya sampai menemukan sebuah nama yang belum terpakai. Kemudian, script membuat sebuah folder dengan nama unik tersebut.
```bash
hour=$(date +%H)
if [ $hour -eq 0 ]
then
    size=1
else
    size=$hour
fi
```
Baris ini menentukan jumlah gambar yang akan diunduh. Jumlah gambar tergantung pada jam pada saat itu. Jika jamnya adalah 0 (yaitu tengah malam), maka hanya satu gambar yang akan diunduh. Jika jamnya tidak 0, maka jumlah gambar yang diunduh adalah sama dengan jam pada saat itu.
```bash
for (( i=1; i<=$size; i++ ))
do
    file_name="perjalanan_$i.jpg"    
    wget -O "$folder_name/$file_name" https://source.unsplash.com/random/800x600/?indonesia
done
```
Baris ini memulai loop untuk mengunduh gambar-gambar dari internet. Untuk setiap gambar, script menghasilkan sebuah nama file baru (misalnya, `perjalanan_1.jpg`), kemudian menggunakan perintah wget untuk mengunduh gambar tersebut dari internet. Gambar-gambar ini akan disimpan di dalam folder yang telah dibuat sebelumnya dengan nama yang unik tersebut.

**Penjelasan zipp.sh**
```bash
cd $PWD
```
Mengambil direktori saat ini sebagai working directory
```bash
folder_num=$(ls -d kumpulan_* | wc -l)
zip_folder=$(ls -d devil_* | wc -l)
zip_name="devil_$(($zip_folder+1)).zip"
```
Baris ini menentukan jumlah folder yang berisi kumpulan gambar dan menentukan nama untuk file zip baru yang akan dibuat. Variabel `folder_num` diisi dengan jumlah folder yang diawali dengan `kumpulan_`, sedangkan variabel `zip_folder` diisi dengan jumlah file zip yang diawali dengan `devil_`. Variabel `zip_name` diisi dengan nama file zip baru yang diawali dengan `devil_` dan diikuti dengan nomor urut yang sesuai.
```bash
for ((i=1; i<=$folder_num; i++))
do
    zip -r $zip_name kumpulan_$i
    rm -r kumpulan_$i
done
```
Baris ini memulai loop untuk mengompres setiap folder `kumpulan_x` menjadi file zip baru dengan nama `devil_nomor.file`. Perintah zip digunakan untuk mengompres folder tersebut dan `-r` digunakan untuk mengompres secara rekursif (yaitu, mengompres semua file di dalam folder tersebut). Setelah selesai mengompres, script menghapus folder yang telah dikompres dengan perintah `rm -r kumpulan_$i`. Script ini dijalankan setiap 1 hari untuk mengompres semua folder `kumpulan_x` yang ada dan membuat file zip baru dengan nama yang sesuai.

**Screenshot Output**
<br>
![Soal2](/uploads/b0bd0fcd2611508aaa88ec6c894442c2/Soal2.png)

## Soal 3

Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
    - Minimal 8 karakter
    - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    - Alphanumeric
    - Tidak boleh sama dengan username
    - Tidak boleh menggunakan kata `chicken` atau `ernie`

- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : `YY/MM/DD hh:mm:ss MESSAGE`. Message pada log akan berbeda tergantung aksi yang dilakukan user.
    - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah `REGISTER: ERROR User already exists`
    - Ketika percobaan register berhasil, maka message pada log adalah `REGISTER: INFO User USERNAME registered successfully`
    - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah `LOGIN: ERROR Failed login attempt on user USERNAME`
    - Ketika user berhasil login, maka message pada log adalah `LOGIN: INFO User USERNAME logged in`

## louis.sh
```bash
#!/bin/bash
echo "==== REGISTER YOUR ACCOUNT HERE ===="

# -q untuk menghilangkan print dari grep, -o untuk hanya cocokkan dengan pola pencarian, dan -w untuk hanya cocokkan kata utuh/lengkap, -r untuk read input dan tidak peduli terhadap input selain alphanumeric

while true
do
  echo "Input new username: "
  read -r username
  if grep -qow "$username" user/user.txt
  then
    echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
    echo "Failed, users already exists! Enter another username"
  else
    break
  fi
done

while true
do
  echo "Input new password: "
  read -rs password
  if [[ $password =~ "chicken" ]] || [[ $password =~ "ernie" ]]
    then
    echo "Password cannot be contain to 'chicken' or 'ernie'"
  elif [[ "$password" == "$username" ]]
    then
    echo "Password cannot same as username"
  elif ! [[ ${#password} -gt 8 && "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] ]]
    then
    echo "Password must contain at least 8 character, 1 uppercase letter, 1 lowercase letter, and contains a number"
  else
    echo "Registration Successfully"
    echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
    echo "$username+$password" >> user/user.txt
    break
  fi
done
```

**Penjelasan**

1. Perulangan dilakukan secara terus-menerus hingga username dan password sesuai dengan kriteria yang ditentukan
2. `read -r` berarti read input bisa apapun, tidak hanya alphanumeric
3. `grep -qow` mencari `username` dalam file `user/user.txt` (`-q` tanpa mencetak, `-o` cocokkan dengan pola pencarian saja, dan `-w` cocokkan dengan kata seutuhnya). Jika username sudah terdaftar, maka input username dengan kata yang berbeda. Jika berhasil, maka fungsi while akan diberhentikan dengan `break`
5. Jika username sudah ada dalam file `user/user.txt`, maka cetak log `$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists` sesuai format dan masukkan ke dalam `log.txt`
4. `read -rs password` berarti input menerima apapun, tidak hanya alphanumeric dan `s` berarti input berupa tipe password
5. Conditional untuk password dilakukan dari persyaratan yang spesifik hingga general agar mendapatkan hasil yang optimal
6. Conditional dilakukan pertama kali dengan kata `chicken` atau `ernie` dikarenakan kata tersebut sangat spesifik
7. Selanjutnya, conditional `password` tidak boleh sama dengan `username `dilakukan kedua karena kata username sangat spesifik
8. Terakhir, conditional persyaratan `password > 8`, `mengandung setidaknya 1 huruf kapital, 1 huruf kecil, dan 1 angka` adalah yang terakhir karena persyaratan ini adalah yang paling general.
9. Jika register berhasil, maka cetak log `(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully` dan masukkan `username` dan `password` ke dalam `user/user.txt` dengan format `$username+$password`

## retep.sh
```bash
#!/bin/bash
echo "==== LOGIN HERE ===="

while true
do
  echo "Username: "
  read -r username
  echo "Password: "
  read -rs password
  if grep -owq "${username}+${password}" user/user.txt
    then
    echo "Login successful as $username"
    echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
    break
  else
    echo "Invalid username/password, please try again"
    echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  fi
done
```
**Penjelasan**
1. `while true` akan terus berjalan hingga username dan password sesuai dengan data di `user/user.txt`
2. `read -r username` berarti read input username bisa apapun, tidak hanya alphanumeric
3. `read -rs password` input `-s` menandakan bahwa input bertipe password
4. `grep -owq "${username} ${password}" user/user.txt` digunakan untuk pengecekan apakah username dan password sudah sesuai
  - `-o` untuk hanya cocokkan dengan pola pencarian `${username}+${password}`
  - `-w` untuk cocokkan dengan kata utuh/lengkap
  - `-q` untuk menghilangkan output/print dari syntax `grep`
5. Jika login gagal, maka masukkan kembali username dan password yang sesuai, kemudian cetak log dan masukkan ke dalam file `log.txt`
6. Jika login berhasil, maka cetak log dan masukkan ke dalam file `log.txt` dengan format yang sudah ditentukan

## Soal 4

Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
- - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
- - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
- - Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀.


## log_encrypt.sh
```bash
#!/bin/bash

input="/var/log/syslog"
path=`readlink -f "${BASH_SOURCE:-$0}"`
DIR_PATH=`dirname $path`

# Crontab, run once, comment/delete after. Or, add cronjob with crontab -e
crontab -l > run_cron
echo "0 */2 * * * sudo $DIR_PATH/log_encrypt.sh" >> run_cron
crontab run_cron

output="$DIR_PATH/$(date +"%H:%M_%d:%m:%Y").txt"
touch "$output"

key=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ
shift=$(date +%H)%26

tr "${key:0:26}${key:52:26}" "${key:${shift}:26}${key:(52+${shift}):26}" < "$input" > "$output"
```

**Penjelasan**

1. Pertama kita definisikan input file kita yaitu syslog yang ingin kita enkripsi
2. Kemudian, kita membaca direktori path kita dengan `readlink -f "${BASH_SOURCE:-$0}"` kemudian mencari parent direktori nya dengan `dirname $path`
3. Tujuan kita mencari parent directory `DIR_PATH` adalah untuk memberitahu dimana posisi shell script kita kemudian masukkan pada crontab.
4. `0 */2 * * * sudo $DIR_PATH/log_encrypt.sh` memiliki makna jalankan log_encrypt.sh dengan sudo privileges setiap 2 jam
5. Kemudian, setelah kita mendefinisikan crontab, kita perlu mendefinisikan output path kita dan memanggil command `touch` untuk membuat filenya
6. Di variabel `key` terdapat kunci dari enkripsi yang perlu kita lakukan
7. Kemudian, variabel shift menyimpan rotation number kita.
8. Kemudian, dengan menggunakan command `tr` kita dapat swap semua huruf dengan huruf yang dienkripsi.
9. `${key:0:26}` merupakan command yang mirip slice yang mengambil setiap huruf dari index 0 sampai 25. Begitu juga dengan yang lainnya.
10. Pada akhirnya, jika sekarang adalah hour 12, maka line terakhir akan menjadi `tr "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "mnopqrstuvwxyzabcdefghijklMNOPQRSTUVWXYZABCDEFGHIJKL"`
11. Setiap huruf a menjadi m, A menjadi M, c menjadi o, C menjadi O

## log_decrypt.sh
```bash
path=`readlink -f "${BASH_SOURCE:-$0}"`
DIR_PATH=`dirname $path`
filename=$(ls -1t *.txt | head -1)

input="$DIR_PATH/$filename"
output="$DIR_PATH/$(basename "${filename%.*}")_decrypted.txt"

mkdir -p "decrypted"
touch "$output"

shift=$(expr $(echo "$filename" | cut -c1-2))

key=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ

tr "${key:${shift}:26}${key:(${shift}+52):26}" "${key:0:26}${key:52:26}" < "$input" > "$output"
```

**Penjelasan**
1. Sama seperti `log_encrypt.sh`, `DIR_PATH` berisi direktori parent dari `log_decrypt.sh`
2. Kemudian kita ingin mencari file untuk di decryot, kita menjalankan command `ls` untuk list semua direktori, tapi kita urutkan dari latest created date dengan `-1t`
3. `*.txt` filter semua file yang memiliki extension .txt kemudian `haed -1` mengambil yang paling atas.
4. Setelah semua itu, `filename` akan berisi file dengan extension .txt yang paling baru dibuat untuk kita decrypt.
5. Setelah mendapat file yang kita ingin decrypt, kita definisikan itu sebagai `input` kemudian kita definisikan output file kita yang akan memiliki `_decrypted` pada filanemanya.
6. Kita akan menyimpan file tersebut dalam folder `decrypted` dan membuat filenya dengan `touch`
7. Untuk mencari tau seberapa banyak kita perlu rotate/shift, kita membaca filename tersebut.
8. Pertama, kita slice filename tersebut dengan command `cut` dengan parameter `-c1-2` yang berfungsi untuk mengambil 2 karater pertama pada filename.
9. Kemudian, kita cast sebagai integer dengan command `expr`
10. Sama seperti pada `log_encrypt.sh`, dengan menggunakan command `tr` kita dapat swap semua huruf dengan huruf yang dienkripsi.
`${key:0:26}` merupakan command yang mirip slice yang mengambil setiap huruf dari index 0 sampai 25. Begitu juga dengan yang lainnya.
11. Pada akhirnya, jika filename di enkripsi pada jam 12, maka line terakhir akan menjadi `tr "mnopqrstuvwxyzabcdefghijklMNOPQRSTUVWXYZABCDEFGHIJKL" "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"`
11. Setiap huruf m menjadi a, M menjadi A, o menjadi c, O menjadi C.
