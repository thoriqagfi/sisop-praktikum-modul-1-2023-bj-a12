#!/bin/bash
echo "==== LOGIN HERE ===="

while true
do
  echo "Username: "
  read -r username
  echo "Password: "
  read -rs password
  if grep -owq "${username}+${password}" user/user.txt
    then
    echo "Login successful as $username"
    echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
    break
  else
    echo "Invalid username/password, please try again"
    echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  fi
done

