#!/bin/bash

# A. Tampilkan 5 universitas ranking tertinggi di jepang
awk -F, '$4=="Japan" {print $2}' 2023\ QS\ World\ University\ Rankings.csv | head -5 | sort


# B. Faculty student score (FSR) paling rendah diantara 5 universitas filter poin A
awk -F, '$4=="Japan" {print $9}' 2023\ QS\ World\ University\ Rankings.csv | head -5 | sort | head -1

# C. 10 Universitas di jepang dengan employment outcome rank tertinggi
awk -F, '$4=="Japan" {print $20,$2}' 2023\ QS\ World\ University\ Rankings.csv | sort -n | head -10

# D. Universitas paling keren di dunia
grep -i keren  2023\ QS\ World\ University\ Rankings.csv | cut -d',' -f2
